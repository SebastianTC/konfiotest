//
//  KonfioTestUITests.swift
//  KonfioTestUITests
//
//  Created by Sebax TC on 24/11/21.
//

import XCTest

class KonfioTestUITests: XCTestCase {
    
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
       
    }

    func testListHasElementsShown() throws {
        // given
        let app = XCUIApplication()
        app.launch()

        // when
        app.swipeUp(velocity: .slow)

        // then
        XCTAssertTrue(isVisibleSomeItem())
    }
    
    func isVisibleSomeItem() -> Bool {
        return XCUIApplication().windows.element(boundBy: 1).exists
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}

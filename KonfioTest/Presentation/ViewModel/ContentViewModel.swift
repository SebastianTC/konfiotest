//
//  ContentViewModel.swift
//  KonfioTest
//
//  Created by Sebax TC on 24/11/21.
//

import Foundation
import Combine

class ContentViewModel: ObservableObject {
    @Published var dogs = [DogModel]()
    @Published var isLoading = false
    private var getDogsUseCase = GetDogs()
    private var saveDogsUseCase = SaveResponseDogs()
    
    func getDogs() {
        isLoading = true
        let _ = getDogsUseCase.execute().subscribe(
            onNext: { response in
                DispatchQueue.main.async {
                    self.dogs = response
                    self.saveDogsUseCase.execute(dogs: response)
                    self.isLoading = false
                }
            })
    }
}

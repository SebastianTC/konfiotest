//
//  ItemView.swift
//  KonfioTest
//
//  Created by Sebax TC on 25/11/21.
//

import SwiftUI
import URLImage

struct ItemView: View {
    @State var item: DogModel = DogModel()
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            HStack(alignment: .bottom) {
                if let url = URL(string: item.image) {
                    URLImage(url,
                             empty: {
                        EmptyView()
                    },
                             inProgress: { progress in
                    },
                             failure: { error, retry in
                        Image("imageNotFound")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 120, height: 120)
                    },
                             content: { image, info in
                        HStack(alignment: .bottom) {
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 140)
                                .cornerRadius(8)
                        }
                        .background(Color.white)
                        .cornerRadius(8, corners: [.topLeft, .topRight, .bottomLeft])
                    })
                } else {
                    Image("ic_default")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 120, height: 120)
                }
                
                HStack(alignment: .bottom) {
                    Spacer()
                    VStack(alignment: .leading) {
                        Spacer()
                        Text(item.dogName)
                            .foregroundColor(Color("blackColor"))
                            .font(.system(size: 20, weight: .regular))
                            .padding(.leading, 8)
                            .padding(.trailing, 8)
                            .multilineTextAlignment(.leading)
                        Text(item.description)
                            .foregroundColor(Color("grayColor"))
                            .font(.system(size: 14))
                            .padding(.top, 2)
                            .padding(.leading, 8)
                            .padding(.trailing, 8)
                            .multilineTextAlignment(.leading)
                        Text("Almost " + String(item.age) + " years")
                            .foregroundColor(Color("blackColor"))
                            .font(.system(size: 12, weight: .regular))
                            .padding(.top, 2)
                            .padding(.leading, 8)
                            .padding(.trailing, 8)
                            .multilineTextAlignment(.leading)
                        Spacer()
                    }
                    .background(Color.white)
                    .padding(.top, 8)
                    .padding(.bottom, 8)
                    .frame(width: 200, height: 180, alignment: .leading)
                    .edgesIgnoringSafeArea(.all)
                    Spacer()
                }
                .background(Color.white)
                .cornerRadius(8, corners: [.topRight, .bottomRight])
            }
            .background(Color("lightGray"))
        }
    }
}

//
//  ContentView.swift
//  KonfioTest
//
//  Created by Sebax TC on 24/11/21.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel: ContentViewModel = ContentViewModel()
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack() {
                    ZStack {
                        if (viewModel.dogs.count > 0) {
                            List {
                                ForEach (viewModel.dogs, id: \.id) { item in
                                    ItemView(item: item)
                                        .listRowSeparator(.hidden)
                                        .listRowBackground(Color("lightGray"))
                                        .padding(.bottom, 24)
                                  }
                            }
                            .accessibility(identifier: "mainList")
                            .listStyle(PlainListStyle())
                        }
                        else if (!$viewModel.isLoading.wrappedValue) {
                            VStack {
                                Text("😞")
                                    .font(.system(size: 44))
                                Text("No hay información")
                                    .multilineTextAlignment(.center)
                            }
                        }
                    }
                    .padding(.top, 4)
                    .frame(minWidth: 0,
                           maxWidth: .infinity,
                           minHeight: 0,
                           maxHeight: .infinity
                    )
                    .onAppear {
                        viewModel.getDogs()
                    }
                    .blur(radius: viewModel.isLoading ? 3 : 0)
                }
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarTitle("Dogs We Love")
            }
            .background(Color("lightGray"))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

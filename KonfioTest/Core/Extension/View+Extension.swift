//
//  View+Extension.swift
//  KonfioTest
//
//  Created by Sebax TC on 25/11/21.
//

import Foundation
import SwiftUI

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

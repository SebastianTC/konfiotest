//
//  KonfioTestApp.swift
//  KonfioTest
//
//  Created by Sebax TC on 24/11/21.
//

import SwiftUI

@main
struct KonfioTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

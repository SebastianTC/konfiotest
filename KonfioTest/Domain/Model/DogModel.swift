//
//  DogModel.swift
//  KonfioTest
//
//  Created by Sebax TC on 25/11/21.
//

import Foundation

class DogModel: Identifiable, Decodable {
    var dogName: String!
    var description: String!
    var age: Int!
    var image: String!
}

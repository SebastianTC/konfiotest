//
//  SaveResponseDogs.swift
//  KonfioTest
//
//  Created by Sebax TC on 27/11/21.
//

import Foundation
import Foundation
import Combine
import RxSwift

class SaveResponseDogs {
    
    private let repository = DogsRepository()
    
    func execute(dogs: [DogModel]) {
        return repository.saveDogs(dogs: dogs)
    }
}

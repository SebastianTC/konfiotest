//
//  GetDogs.swift
//  KonfioTest
//
//  Created by Sebax TC on 25/11/21.
//

import Foundation
import Combine
import RxSwift

class GetDogs {
    
    private let repository = DogsRepository()
    
    func execute() -> Observable<[DogModel]> {
        return repository.performGetDogs()
    }
}

//
//  DogListEntity.swift
//  KonfioTest
//
//  Created by Sebax TC on 27/11/21.
//

import Foundation
import RealmSwift

class DogListEntity: Object {
    @objc dynamic var id = "" 
    @Persisted var dogs = List<DogEntity>()
}

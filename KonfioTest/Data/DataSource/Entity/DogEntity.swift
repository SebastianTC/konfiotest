//
//  DogEntity.swift
//  KonfioTest
//
//  Created by Sebax TC on 27/11/21.
//

import Foundation
import RealmSwift

class DogEntity: Object {
    @Persisted var dogName: String
    @Persisted var age: Int
    @Persisted var descriptionDog: String
    @Persisted var image: String
}

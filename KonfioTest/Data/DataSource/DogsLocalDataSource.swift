//
//  DogsLocalDataSource.swift
//  KonfioTest
//
//  Created by Sebax TC on 25/11/21.
//

import Foundation
import RxSwift
import RealmSwift

class DogsLocalDataSource {

    func performGetDogs() -> Observable<[DogModel]> {
        return Observable<[DogModel]>.create { observer in
            var dogs = [DogModel]()
            
            let realm = try! Realm()
            guard let results = realm.objects(DogListEntity.self).first else {
                observer.onNext(dogs)
                return Disposables.create()
            }
            
            for dogEntity in results.dogs {
                let dogModel = DogModel()
                dogModel.dogName = dogEntity.dogName
                dogModel.age = dogEntity.age
                dogModel.description = dogEntity.descriptionDog
                dogModel.image = dogEntity.image
                dogs.append(dogModel)
            }
            
            observer.onNext(dogs)
            
            return Disposables.create()
        }
    }
    
    func saveDogs(dogs: [DogModel]) {
        let dogsList = List<DogEntity>()
        let dogListEntity = DogListEntity()
        
        for dog in dogs {
            let dogEntity = DogEntity()
            dogEntity.dogName = dog.dogName
            dogEntity.age = dog.age
            dogEntity.descriptionDog = dog.description
            dogEntity.image = dog.image
            dogsList.append(dogEntity)
        }
        
        dogListEntity.id = UUID().uuidString
        dogListEntity.dogs = dogsList
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(dogListEntity)
        }
    }
    
    func isSaved() -> Bool {
        let realm = try! Realm()
        guard let _ = realm.objects(DogListEntity.self).first else {
            return false
        }
        
        return true
    }
}

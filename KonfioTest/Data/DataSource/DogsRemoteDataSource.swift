//
//  DogsRemoteDataSourse.swift
//  KonfioTest
//
//  Created by Sebax TC on 25/11/21.
//

import Foundation
import RxSwift

class DogsRemoteDataSource: ObservableObject {
    
    func performGetDogs(url: URL = Constants.endPoint) -> Observable<[DogModel]> {
        return Observable<[DogModel]>.create { observer in
            var dogs = [DogModel]()
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    switch error {
                    case .some(let error as NSError):
                        observer.onError(error)
                    case .none:
                        observer.onNext(dogs)
                    }
                    return
                }
                
                self.parseJson(data: data) { dogModels in
                    dogs = dogModels
                }
                
                observer.on(.next(dogs))
            }
            
            task.resume()
            
            return Disposables.create()
        }
    }
    
    func parseJson(data: Data, completion: @escaping (([DogModel]) -> Void)) {
        var dogs = [DogModel]()
        if let tmpDogs = try? JSONDecoder().decode(
            [DogModel].self,
            from: data
        ) {
          dogs = tmpDogs
        }
        
        completion(dogs)
    }
}

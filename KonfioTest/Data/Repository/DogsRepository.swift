//
//  DogsRepository.swift
//  KonfioTest
//
//  Created by Sebax TC on 25/11/21.
//

import Foundation
import RxSwift
import RealmSwift

class DogsRepository {
    private var remoteDataSource = DogsRemoteDataSource()
    private var localDataSource = DogsLocalDataSource()
    
    func performGetDogs() -> Observable<[DogModel]> {
        if localDataSource.isSaved() {
            return localDataSource.performGetDogs()
        }
        else {
            return remoteDataSource.performGetDogs()
        }
    }
    
    func saveDogs(dogs: [DogModel]) {
        if !localDataSource.isSaved() {
            localDataSource.saveDogs(dogs: dogs)
        }
    }
}

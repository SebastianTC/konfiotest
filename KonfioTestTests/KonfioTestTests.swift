//
//  KonfioTestTests.swift
//  KonfioTestTests
//
//  Created by Sebax TC on 24/11/21.
//

import XCTest
@testable import KonfioTest

class KonfioTestTests: XCTestCase {
    var sut: DogsRemoteDataSource!
    let networkMonitor = NetworkMonitor.shared
    var itemsToShow = 0

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = DogsRemoteDataSource()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testApiCallCompletes() throws {
        try XCTSkipUnless(
          networkMonitor.isReachable,
          "Network connectivity needed for this test."
        )

        // given
        let url = Constants.endPoint
        let promise = expectation(description: "Completion handler invoked")
        var responseError: Error?

        // when
        let _ = sut.performGetDogs(url: url).subscribe { dogs in
            self.itemsToShow = dogs.count
            promise.fulfill()
        } onError: { error in
            responseError = error
            promise.fulfill()
        } onCompleted: {
            
        } onDisposed: {
            
        }
        
        wait(for: [promise], timeout: 5)

        // then
        XCTAssertNil(responseError)
        XCTAssertNotEqual(itemsToShow, 0)
    }
}
